'use strict';
const browserSync = require('browser-sync').create();

module.exports = ({pathServer, pathWatch}) => {
  return () => {
    browserSync.init({
      server: pathServer
    });
    
    browserSync.watch(pathWatch).on('change', browserSync.reload);
  }
};