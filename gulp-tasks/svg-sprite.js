'use-strict';

const gulp = require('gulp'),
      svgstore = require('gulp-svgstore'),
      svgmin = require('gulp-svgmin'),
      rename = require('gulp-rename');

module.exports = ({src, output}) => {
  return () => {
    return gulp.src(src)
      .pipe(svgmin({
        plugins: [{
          removeViewBox: false
        }]
      }))
      .pipe(svgstore())
      .pipe(rename('symbols.svg'))
      .pipe(gulp.dest(output));
  };
};
