'use strict';
const gulp = require('gulp'),
      newer = require('gulp-newer');

module.exports = ({src, taskName, output}) => {
  return () => {
    return gulp.src(src, { since: gulp.lastRun(taskName) })
     .pipe(newer(output))
     .pipe(gulp.dest(output));
  }
};