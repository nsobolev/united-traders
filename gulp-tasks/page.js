'use strict';

const gulp = require('gulp'),
      posthtml = require('gulp-posthtml'),
      include = require('posthtml-include');

module.exports = ({src, output}) => {
  return () => {
    return gulp.src(src)
      .pipe(posthtml([
          include()
      ]))
      .pipe(gulp.dest(output));
  };
};