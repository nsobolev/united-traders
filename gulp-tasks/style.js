'use strict';
const gulp = require('gulp'),
      gulpIf = require('gulp-if'),
      plumber = require('gulp-plumber'),
      notify = require('gulp-notify'),
      less = require('gulp-less'),
      glob = require('less-plugin-glob'),
      postcss = require('gulp-postcss'),
      autoprefixer = require('autoprefixer'),
      mqpacker = require('css-mqpacker'),
      postcssinlinesvg = require('postcss-inline-svg'),
      csso = require('gulp-csso'),
      rename = require('gulp-rename'),
      postcssflexbugsfixes = require('postcss-flexbugs-fixes'),
      sourcemaps = require('gulp-sourcemaps');

module.exports = ({src, output, isDevelop}) => {
    return () => {
        return gulp.src(src)
          .pipe(plumber({
            errorHandler: notify.onError((err) => {
              return {
                title: 'Styles',
                message: err.message
              }
            })
          }))
          .pipe(gulpIf(isDevelop, sourcemaps.init()))
          .pipe(less({
            plugins: [ glob ]
          }))
          .pipe(postcss([
            autoprefixer(),
            postcssflexbugsfixes(),
            postcssinlinesvg(),
            mqpacker({ sort: true })
          ]))
          .pipe(csso())
          .pipe(gulpIf(isDevelop, sourcemaps.write()))
          .pipe(rename('style.min.css'))
          .pipe(gulp.dest(output));
    }
  };