# United Traders

## Разработка CSS - в режиме dev
Команда: 
```
yarn dev
```
- Рабочая директория: src/styles/
- Стартовый файл - src/styles/bootstrap.less
- Компоненты: src/styles/components
- Общие стили для сайта: src/styles/common

### Разработка JS - в режиме dev
Команда: 
```
yarn start
```
- Рабочая директория: src/js/
- Основной файл: src/js/index.js
- Компоненты: src/js/components/

### Сборка в прод (CSS + JS) 
Команда: 
```
yarn build
```
- Собирается в папку dist/

### Сборка спрайтов
Команда: 
```
yarn sprite
```
- Директория для иконок: src/assets/svg/sprite
- Соберется в: dist/svg/symbols.svg
- При необходимости расширяем сборку спрайта
