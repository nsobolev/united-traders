const gulp = require('gulp');

// Variables
const isDevelop = !process.env.NODE_ENV ||  process.env.NODE_ENV == 'development';

const paths = {
    styles: {
      src: './src/styles/style.less',
      output: 'dist/css/'
    },
    html: {
      src: './src/html/*.html',
      output: 'dist'
    },
    svg: {
      src: './src/assets/svg/sprite/*.*',
      output: 'dist/svg'
    },
    include: {
      src: './gulp-tasks'
    },
    default: {
      output: 'dist'
    }
  };

const lazyRequireTask = (taskName, path, options) => {
  options = options || {};
  options.taskName = taskName;
  gulp.task(taskName, (callback) => {
    let task = require(path).call(this, options);
    return task(callback);
  });
};

// Frontend
lazyRequireTask('style', `${paths.include.src}/style`, {
  src: paths.styles.src,
  output: paths.styles.output,
  isDevelop: isDevelop
});

lazyRequireTask('html', `${paths.include.src}/page`, {
  src: paths.html.src,
  output: paths.html.output,
});

lazyRequireTask('sprite', `${paths.include.src}/svg-sprite`, {
  src: paths.svg.src,
  output: paths.svg.output,
});

lazyRequireTask('serve', `${paths.include.src}/serve`, {
  pathServer: paths.default.output,
  pathWatch: `${paths.default.output}/**/*.*`
});

// Works directory and files
lazyRequireTask('copy', `${paths.include.src}/copy`, {
  src: ['./src/assets/{fonts,image}/**/*.*', './src/assets/svg/*.svg'],
  output: paths.default.output
});

// Watches
gulp.task('watch', () => {
  gulp.watch('./src/styles/**/*.*', gulp.series('style'));
  gulp.watch(['./src/assets/{fonts,image}/**/*.*', './src/assets/svg/*.svg'], gulp.series('copy'));
  gulp.watch('./src/html/**/*.*', gulp.series('html'));
});

// Modes
gulp.task('build', gulp.series(
   gulp.parallel('style', gulp.series('html', 'copy', 'sprite')))
);

gulp.task('dev', gulp.series('build', gulp.parallel('watch', 'serve')));
