export default class OpenModal {
  constructor(
    containerSelector = '.main-header__container-menu',
    containerClassOpen= 'main-header__container-menu_state_open',

    buttonSelector = '.button-menu',
    buttonClassOpen = 'button-menu_state_open',
  ) {
    const container = document.querySelector(containerSelector);
    const button = document.querySelector(buttonSelector);

    if (!container || !button) throw new Error('Не найден контейнер или кнопка.');

    this.container = container;
    this.containerClassOpen = containerClassOpen;

    this.button = button;
    this.buttonClassOpen = buttonClassOpen;

    this.init();
  }

  init() {
    this.button.addEventListener('click' , () => {
      this.container.classList.toggle(this.containerClassOpen);
      this.button.classList.toggle(this.buttonClassOpen);
    });
  }
}
