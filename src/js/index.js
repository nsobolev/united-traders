import OpenDialog from './components/OpenModal';

document.addEventListener('DOMContentLoaded', () => {
  const menuContainer = document.querySelector('.main-header__container-menu');
  if (menuContainer) {
    new OpenDialog();
  }
});
