const path = require('path');

module.exports = (env) => {
  return {
    context: path.resolve(__dirname, './src/js'),
    mode: 'development',
    watch: !!(env && env.dev),
    devtool: env && env.dev ? 'eval' : '',

    entry: {
      bundle: './index.js',
    },
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, './dist/js')
    },

    devServer: {
      port: 8080,
      contentBase: path.resolve(__dirname, './dist'),
      publicPath: '/js/',
      hot: true
    },

    module: {
      rules: [ 
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
              plugins: ['@babel/plugin-proposal-class-properties']
            }
          }
        },
      ]
    }
  }
};
